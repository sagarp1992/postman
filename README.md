Postman
=======
Check your api Here

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist sagarp1992/yii2-postman "*"
```

or add

```
"sagarp1992/yii2-postman": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \sagar\postman\AutoloadExample::widget(); ?>```