<?php

namespace vendor\sagarp1992\postman\controllers;

use yii\web\Controller;

/**
 * Default controller for the `postman` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
